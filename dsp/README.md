# Dataspace Protocol deployment

The TSG components for the dataspace protocol consist of 5 helm charts you need to deploy. The first one is a database for all the components, the second one is Casdoor, an oauth provider like KeyCloak, the last 3 ones are the dataspace components namely the Wallet, Control Plane and the Data Plane.

This repository shows the example for the [Meter Data Service](https://meter-data-service-example.dsp.enershare.dataspac.es/#/login). Of course, sensitive info is redacted.

## 1. Postgres

To deploy the database you can simply edit the Username and Password variables in `values.postgres.yaml`, and deploy the chart. This will setup a postgres instance and create the databases for the other components. It will also create a secret which the other values files will use to authenticate to the database.

```
helm upgrade  --create-namespace --install -f meter-data-service/values.postgres.yaml -n dsp --repo https://charts.bitnami.com/bitnami --version 13.4.0 meter-data-service-postgresql postgresql
```

## 2. Casdoor

For Casdoor, you can change the username and password (which are REDACTED right now) to create users for the components to communicate between each other and to provide a user login. The deployed `casdoor-init` service will create kubernetes secrets for the other components so you do not have to worry about these usernames/passwords in the other values files.

```
helm upgrade  --install -f meter-data-service/values.casdoor.yaml -n dsp --repo https://nexus.dataspac.es/repository/dsp-stable --version 0.1.2 meter-data-service-casdoor casdoor
```

## 3. Wallet

For the Wallet, you need a Pre Authorization code. For this, communicate your `did:web` key to the adminstrators of the Enershare dataspace or contact them how to obtain a pre-authorized code. With this pre-authorized code, you can deploy the wallet chart. Fill the code where it says REDACTED in the values file.

```
helm upgrade  --install -f meter-data-service/values.wallet.yaml -n dsp --repo https://nexus.dataspac.es/repository/dsp-stable --version 0.1.2 meter-data-service-tsg-wallet tsg-wallet
```

## 4. Control Plane


For the control plane, simply update the parts where it says Meter Data Service, to comply it to the service you have and you are ready to deploy it.

```
helm upgrade --wait --install -f meter-data-service/values.controlplane.yaml -n dsp --repo https://nexus.dataspac.es/repository/dsp-stable --version 0.1.2 meter-data-service-tsg-control-plane tsg-control-plane
```

## 5. Data Plane

For the data plane, simply update the parts where it says Meter Data Service, to comply it to the service you have and you are ready to deploy it.

```
helm upgrade  --install -f meter-data-service/values.dataplane.yaml -n dsp --repo https://nexus.dataspac.es/repository/dsp-stable --version 0.1.2 meter-data-service-tsg-http-data-plane tsg-http-data-plane
```