# Enershare

## Deployment checklist

### 1. Setup deployment environment

Feel free to send us an email with questions on your (desired) environment, given the large amount of possibilities listing them all here is not feasible.

a. Kubernetes

  1. Single or multi-node cluster, self-hosted (via Minikube, Kubeadmm etc, making sure public IP address(es) are available for ingress to the cluster) or leveraging cloud providers offerings (Azure Kubernetes Service, Google Kubernetes Engine, Amazon Elastic Kubernetes Service)  
  2. Nginx Ingress Controller combined with certificate handling (e.g. Cert Manager)

b. Docker (discouraged)

  1. Single node, but requiring a public IP address
  2. Ingress or reverse proxy

### 2. Request dataspace certificates

1. Create an account at the [Enershare Identity Provider](https://daps.enershare.dataspac.es/#management)
2. Go to the sub-tab Participants within the Management tab. Next, request a Participant certificate via the button at the bottom of the page. You can choose your own participant ID, our suggestion is to use an identifier in the form of urn:ids:enershare:participants:ORGANISATION_NAME (where spaces can be replaced with hyphens).
You can either choose to create a private key and certificate signing request via the OpenSSL CLI or use the form under "Create private key in browser". When using the form to generate the private key, ensure you download it, since it won't be stored anywhere by default.
3. Go to the sub-tab Connectors withing the Management tab. And request a Connector certificate. The process is similar to the Participant certificate, but it requires a participant certificate. For the connector ID, our suggestion is to use an identifier in the form of urn:ids:enershare:connectors:ORGANISATION_NAME:CONNECTOR_NAME (where spaces can be replaced with hyphens). The connector name can be a human readable name for the connector. In the end you should have a connector.key, connector.crt, and cachain.crt file that you'll use for the connector configuration.
4. Send a message to Maarten and Willem, or ask during one of the calls, to activate the certificates.

### 3. Deploy connector

> _Note:_ The commands below assume deployments in a namespace called `enershare-mvp`, this can be changed to any other namespace.

The connector can almost be deployed. This section shows how to integrate the certificate into kubernetes and customize the `values.yaml` file to set up a connector and a OpenAPI Data App. The [OpenAPI data app](https://gitlab.com/tno-tsg/data-apps/openapi) offers two modes of which the consumer mode is considered easiest to deploy. The consumer-only data app allows a party to connect to other party's OpenAPI service over the Data Space infrastructure. After setting it up, you'll be provided a web UI that offers a description of the API calls you can do and test them out. This is the consumer scenario and this is described in paragraph 3.2.

The provider scenario also allows you to serve your own OpenAPI applications and it needs some more setting up, though the values file also guides you along the way.

With respect to the terminology that follows, note that one connector can be re-used by multiple applications called agents. In a simpler one-to-one scenario there is only one connector and only one agent, such as the consumer connector with a single consumer agent.
On the other hand, with the data app in client & server mode, the single connector is used by a provider agent as well as by a consumer agent and the helm chart sets up both agents.

For more information on the TNO Data App please consult the [TNO IDS OpenAPI Data App Readme](https://gitlab.com/tno-tsg/data-apps/openapi/-/blob/master/README.md?ref_type=heads).

#### 3.1 Create certificate secret

Create secret with the certificate created in step 2.3 (make sure you are in the directory that contains the component.crt, component.key, and cachain.crt files):

``` bash
kubectl create namespace enershare-mvp

kubectl create secret generic \
        ids-identity-secret \
        -n enershare-mvp \
        --from-file=ids.crt=./component.crt \
        --from-file=ids.key=./component.key \
        --from-file=ca.crt=./cachain.crt
```

#### 3.2 Deploy consumer connector

The `values.yaml` file in this repository can be used as basis for the consumer, make sure to change all the lines that include "# CHANGE" at the end of the line. A list of changes required:

* Line 1: DNS hostname that points to your ingress controller
* Line 27, 28, 29: To the identifiers created in step 2.2 & 2.3
* Line 31, 33: To the connector name created in step 2.3, and optionally a more descriptive description of your service in the future
* Line 40 & 70: Change the bit after APIKEY- to a random API key used for interaction between the core container and the data app
* Line 47: Create your own BCrypt encoded password for the admin user of the connector (also used in the default configuration to secure the ingress of the data app)
* Line 75: Create an agent ID, suggested to use the Connector ID from step 2.3 appended with ":AGENT_NAME"
* Line 76: Update the agents title to increase findability in the future

Then deploy the connector by executing the Helm (requires to have Helm installed, see [Helm installation guide](https://helm.sh/docs/intro/install/) upgrade command (should be executed in the same directory as the values.yaml file):

``` bash
helm upgrade --install \
        -n enershare-mvp \
        --repo https://nexus.dataspac.es/repository/tsg-helm \
        --version 3.2.8 \
        -f values.yaml \
        enershare-consumer \
        tsg-connector
```

After successful deployment, the OpenAPI data app interface should be available at `https://YOUR_DOMAIN_NAME/data-app/` with the login matching the admin user with the provided BCrypt password.

Also, after successful deployment, your connector should be available in the [Metadata Broker](https://broker.enershare.dataspac.es/#connectors).

In the OpenAPI data app UI, go to "Tester" and click on "Query" and expand the agent with id `urn:ids:enershare:connectors:MeterDataService:ServiceAgent` and click on "Use". Select a sender agent from the list and provide as path "/powermeters". This should result in a JSON array of observer IDs.

#### 3.3 Deploy provider connector

The `values.yaml` file in this repository can also be used as basis for a provider, with similar changes required as for the consumer.

In lines 77-93 an example of a provider agent can be used as basis. Next to this, also an ingress route should be configured, which is in line 59-64.

### 4. Create/integrate own service(s)

1. Create or re-use OpenAPI specification
2. Develop or re-use services implementing the specification
3. Integrate services into the connector configurations using the provider set-up.

### 5. Policy Enforcement

> _**WARNING**_: Connectors with policy enforcement enabled can only communicate with other connectors that have policy enforcement enabled. 

The `values.pef.yaml` provides a similar configuration as `values.yaml` but includes the necessary configuration to support the negotiation and enforcement of policies. The configuration is independent of other configuration, so no specific configuration is required that is unique to your deployment. This configuration is located at two places:
- **Core Container route configuration** - Explicit egress route is required, with the inclusion of the `policyEnforcement` property. The ingress route for the OpenAPI data app must be updated by including the properties `policyEnforcement` & `delegatedPolicyNegotiation`. This will ensure that policy enforcement is executed on both ingress messages as egress messages.
- **OpenAPI data app configuration** - The `usePolicyEnforcement` property must be set to true, signalling to the data app that it should handle the negotiation of policies.

### 6. Client Side Calls to OpenAPI data app

If you want to make use of the functionality the Open API data app offers in a programmatic way, you can use client side calls to query the Open API data app. This is also what the UI does under the hood. The calls that you can make have the following structure: `https://<baseurl>/<data-app-path>/openapi/<version>/<endpoint>`, where `baseurl` is the url that you deployed your connector on, `data-app-path` is the path you used for the data app. In the default values.yaml that is included in this repository, this is `/data-app/`. Then for the `version` you select the version of your own backend service, and for `endpoint` you choose an endpoint that your service has. An example of the URL for a call that the data app UI does from the Meter Consumer to the Meter Data Service looks as follows: `https://meter-consumer.enershare.dataspac.es/data-app/openapi/0.1.0/measurements`. 

Of course this is only a call from the client to the OpenAPI data app of the consumer. For the OpenAPI data app to know where to route the request, we can give headers with the request. The headers we use are the `Forward-ID`, and the `Forward-Sender` headers. The first header contains the Agent ID of the service that is registered at the party you want to interact with. The latter contains your own Agent ID, to identify yourself. For the same meter data service example this looks as follows:
```
Forward-Id: urn:ids:enershare:connectors:MeterDataService:ServiceAgent
Forward-Sender: urn:ids:enershare:connectors:MeterDataService:ConsumerAgent
```

If you're using external authentication, please note that you probably should not make calls to the OpenAPI data app via the ingress. In this case you can deploy your service in the same kubernetes cluster as the data app. Then use the internal kubernetes service url to access the data app. 


## Large request support

The latest versions of the Core Container and OpenAPI data app have improved support for sending large requests, tested up to 250MB of data.
The memory footprint of large requests will increase for the Core Containers, for sending/receiving requests of 250MB the memory consumption of the Core Container will be ±1.2GB.

## Clearing House support

Support for the clearing houses is provided via the Policy Enforcement. To enable this, the following requirements should be fulfilled:
* Deployment of Connector with Policy Enforcement (see the [section above](#5-policy-enforcement) or the `values.pef.yaml`)

And for providers that want to ensure communication is logged, the following steps should be followed: 
* As provider create policies mentioning the clearing house, e.g. via the _Policy Management_ in the UI add a rule and provide the address for the clearing house connector in the _Clearing Logging_ field.
* For the Enershare Clearing house, the connector address is: `https://clearing-house.eu/clearing`
